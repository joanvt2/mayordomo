import Vue from 'vue';
import VueAwesomeSwiper from 'vue-awesome-swiper';

// require styles
import 'swiper/dist/css/swiper.css';

Vue.use(VueAwesomeSwiper);

Vue.component(
    'services-list',
    require('./components/ServiceComponent.vue')
);

