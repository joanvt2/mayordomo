const mix = require('laravel-mix');

mix.setPublicPath(__dirname+'/public');

mix.copy('src/services','public/services',false);
mix.copy('src/images','public/assets/images',false);
mix.sass('src/scss/app.scss', 'public/assets/css');

mix.ts('src/js/app.js', 'public/assets/js').setPublicPath(__dirname);