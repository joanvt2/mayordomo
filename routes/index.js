var express = require('express');
var app = express();

var path = require('path');

var fs = require('fs');

var mcache = require('memory-cache');

let cache = (duration) => {
    return (req, res, next) => {
        let key = '__express__' + req.originalUrl || req.url;
        let cachedBody = mcache.get(key);
        if (cachedBody) {
            res.send(cachedBody);
            return
        } else {
            res.sendResponse = res.send;
            res.send = (body) => {
                mcache.put(key, body, duration * 1000);
                res.sendResponse(body)
            };
            next()
        }
    }
};


app.get('/', cache(1000), function (req, res) {
    setTimeout(() => {
        res.sendFile(path.resolve('public/index.html'));
    }, 100)
});

app.get('/services', function (req, res) {
    data = fs.readFileSync('public/services/services.json', {encoding: 'utf8'});
    res.set({ 'content-type': 'application/json; charset=utf-8' });
    res.end(data.toString());
});

app.use((req, res) => {
    res.status(404).send('');
});

module.exports = app;
