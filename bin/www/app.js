const hostname = '127.0.0.1';
const port = 3000;
var _ = require('lodash');
var express = require('express');
var app = express();

var index = require('../../routes/index');

app.use('/assets',express.static('public/assets'));

app.use('/', index);

app.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`)
});


